# LEIA-ME #

Esse simulador/editor de circuitos está sendo criado para o projeto de Programação Orientada a Objetos II da Universidade Federal de Santa Catarina (UFSC). 

Como tal, ele será disponibilizado como open-source após a avaliação do projeto. (principalmente se o projeto **realmente** for um simulador de circuitos).

É isso. Divirta-se. 

## Criadores do projeto ##

* Caique Marques
* Emmanuel Podestá
* Fernando Mota