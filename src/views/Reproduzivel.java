package views;

import java.awt.*;

public interface Reproduzivel extends Editavel {
    public void reproduzir(Graphics g);
}
