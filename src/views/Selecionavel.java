package views;

public interface Selecionavel {
    public boolean contemPonto(int x, int y);
    public void selecionaPonto(int x, int y);
}
